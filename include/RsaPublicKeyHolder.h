

#pragma once

#include <mbedtls/md.h>
#include <mbedtls/pem.h>
#include <mbedtls/pk.h>
#include <mbedtls/md.h>
#include <mbedtls/error.h>
#include <mbedtls/sha256.h>
#include <mbedtls/base64.h>

#include <stdexcept>
#include "Utils.h"
#include<string>
#include "Sha256RsaSignature.h"

using namespace std;

class RsaPublicKeyHolder{
private:
    mbedtls_pk_context pk;
public:
    RsaPublicKeyHolder(){};

    void loadKeyFromFile(string publicKeyPath);

    void loadKey(string publicKey);

    void verifySignature(Sha256Checksum& checksum ,Sha256RsaSignature signature);

    void verifySignature(string& m ,Sha256RsaSignature signature);
};



