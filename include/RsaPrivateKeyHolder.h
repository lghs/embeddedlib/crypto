

#pragma once


#include <mbedtls/pk.h>

#include <stdexcept>
#include "Utils.h"
#include "Sha256Checksum.h"
#include "Sha256RsaSignature.h"

#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>
#include<string>

using namespace std;

#ifndef MY_ENTROPY
#define MY_ENTROPY "MyEntropy";
#endif

class RsaPrivateKeyHolder {
private:
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_pk_context pk;
public:
    RsaPrivateKeyHolder();
    void loadKey(string keyPath);
    Sha256RsaSignature sign(Sha256Checksum checksum);
};



