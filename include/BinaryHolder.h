

#pragma once

#include <string>

using namespace std;

class BinaryHolder {
public:
    BinaryHolder(){};

    virtual size_t size();
    virtual char * value();

    void initBlank();

    string base64();
};



