#pragma once

#include<string>
#include "BinaryHolder.h"

using namespace std;

#define SHA256_CHECKSUM_SIZE 32

class Sha256Checksum : public BinaryHolder {
private:
    char valueHolder[32];
public:
    Sha256Checksum(char *value);
    Sha256Checksum();

    size_t size() override { return SHA256_CHECKSUM_SIZE; }

    char *value() override { return valueHolder; }

    static Sha256Checksum of(string m);

};



