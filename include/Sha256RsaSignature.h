#pragma once

#include<string>
#include <mbedtls/bignum.h>
#include "BinaryHolder.h"
#include "Sha256Checksum.h"

using namespace std;

#define RSA_SIGNATURE_MAX_SIZE MBEDTLS_MPI_MAX_SIZE

class Sha256RsaSignature : public BinaryHolder{
private:
    char valueHolder[RSA_SIGNATURE_MAX_SIZE];
public:
    size_t acutalSize = 0;
    Sha256RsaSignature();

    size_t size() override { return acutalSize; }

    char *value() override { return valueHolder; }

    static Sha256RsaSignature ofBase64(string s);
};



