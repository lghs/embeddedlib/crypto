#pragma once

#include<string>
using namespace std;

string publicKey="-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyKaoBUOgTua0ylwekWKa\n7cot8oizV8nDXs7a+F3QMhaWzx6MTebXgNDhYvRL/xTBjOqy944JympS5UwU4+w/\nL7jYFFZ7MHotS+uBN5Lmok/8QjeBU6kRCRXOW0GhWSGXISR2owptEnDvZJd1pgrh\nQi7iXBC/70LEjIdo0hdS/ABQvIFxzgpr2S4//FxkaWo09V40YEL3/QF4ebZUChEv\nZrQzv89Gib9cc1+Dh3MQnheV0xhUxiQMqZes3BmfrrhqS3NYTEblmYhp8SoqFtFa\nAzmDYDsjiKSaU0up5uSfaG/I2NN02VKQQ8lY4334fKvQRzOPnkAgnaf3/Rhl4HAf\nqQIDAQAB\n-----END PUBLIC KEY-----\n";

string message="Hello world";

string messageHash="ZOyIygCyaOW6GjVnihtTFtIS9PNmskdyMlNKiuyjfzw=";

string messageSignature="PoMcM9T/muukB68fxupLTgfg9/5LfH8zuNm9X2/T37bh2jZ0zrcdjb1ODW2zAy23KlvkyMG8ZIX+Ms2mYujOiJrGrAq+iu6Ln99tAkRzMlRSJTBDVKBbsiLF4fZyhG0f0ajHrzBm1T8WtuJnqTrW6rIqFDgd/bM7ehe/Gx7aP+VJWxkxyNm01btwUOSK6YpQ8VfxkyTU8ZjZXLItgxIHkZyHIQKoUmQbmz5JanH7TjYLx7soVbP2h/ViKIvxeRSP2//YvCL0sTUFfNgY7ragYE0R8zOV+ycF4QKPtR6pQazA29eR7xnYEStzyWSkC4Abg2bJdI6AdQPEFxfY/Ghpnw==";
