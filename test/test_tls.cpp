/*
 Copyright (c) 2014-present PlatformIO <contact@platformio.org>
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
**/

#include <unity.h>


#include "data.h"
#include <stdio.h>
#include<string>
#include <iostream>
#include <Sha256Checksum.h>
#include <RsaPrivateKeyHolder.h>
#include "RsaPublicKeyHolder.h"

#define TEST_NO_ERROR(f)            \
try{                                \
    f                               \
} catch (std::runtime_error e){     \
    TEST_FAIL_MESSAGE(e.what());    \
}

using namespace std;

string projectPath;
RsaPublicKeyHolder rsaPublicKeyHolder;
RsaPrivateKeyHolder rsaPrivateKeyHolder;

void setUp(void) {
}

void tearDown(void) {
}

void test_read_rsa_public_key(void) {
    TEST_NO_ERROR(rsaPublicKeyHolder.loadKey(publicKey);)
}


void test_checksum(void) {
    TEST_NO_ERROR(
            Sha256Checksum checksum = Sha256Checksum::of(message);
            TEST_MESSAGE((string("b64 hash ") + checksum.base64()).c_str());
            TEST_ASSERT_EQUAL_CHAR_ARRAY(messageHash.c_str(), checksum.base64().c_str(), messageHash.size());
    )
}

void test_check_signature(void) {

    TEST_NO_ERROR(rsaPublicKeyHolder.verifySignature(message, Sha256RsaSignature::ofBase64(messageSignature));)
}

void test_sign_and_verify(void) {

    rsaPrivateKeyHolder.loadKey(projectPath + "test/key.insecure.pem");
    Sha256Checksum checksum = Sha256Checksum::of(message);
    Sha256RsaSignature sig;
    TEST_NO_ERROR(
             sig = rsaPrivateKeyHolder.sign(checksum);
             rsaPublicKeyHolder.verifySignature(checksum, sig);
    )
    TEST_MESSAGE((string("signature : ") + sig.base64()).c_str());
}

void RUN_UNITY_TESTS() {
    UNITY_BEGIN();
    RUN_TEST(test_read_rsa_public_key);
    RUN_TEST(test_checksum);
    RUN_TEST(test_check_signature);
    RUN_TEST(test_sign_and_verify);
    UNITY_END();
}


int main(int argc, char **argv) {
    string programPath = argv[0];

    projectPath = programPath.substr(0, programPath.find(".pio"));
    RUN_UNITY_TESTS();
    return 0;
}


