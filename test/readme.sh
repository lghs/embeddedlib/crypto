#!/bin/bash

set -ue
set -o pipefail

DIR=$(cd $(dirname $0) && pwd)

INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}

PASS="pass"
MESSAGE="Hello world"

info generate key
openssl genrsa -des3 -passout pass:$PASS -out $DIR/key.pem 2048

info generate cert 
openssl rsa -in $DIR/key.pem -passin pass:$PASS -pubout -out $DIR/cert.pem

info create insecure \(without password\) key
openssl rsa -in $DIR/key.pem -out $DIR/key.insecure.pem -passin pass:$PASS

info sha256 of $MESSAGE
HASH=$(echo -n $MESSAGE | openssl dgst -binary -sha256 | openssl base64 -A)
echo $HASH

info sign $MESSAGE
SIGNATURE=$(openssl dgst -sha256 -sign $DIR/key.insecure.pem <(echo -n $MESSAGE) | base64 | tr -d "\n")

info verify signature
openssl dgst -sha256 -verify $DIR/cert.pem -signature <(echo $SIGNATURE | base64 -d) <(echo -n $MESSAGE)

info export info for test
PUBLIC_KEY=$(cat $DIR/cert.pem| tr '\n' '#' | sed 's/#/\\n/g';)
(
cat << EOF
#pragma once

#include<string>
using namespace std;

string publicKey="${PUBLIC_KEY}";

string message="${MESSAGE}";

string messageHash="${HASH}";

string messageSignature="${SIGNATURE}";
EOF
) > $DIR/data.h
