

#include <stdexcept>
#include <mbedtls/base64.h>
#include "Sha256RsaSignature.h"
#include "Utils.h"

Sha256RsaSignature::Sha256RsaSignature() {
    initBlank();
};

Sha256RsaSignature Sha256RsaSignature::ofBase64(string s) {
    Sha256RsaSignature sig;
    int ret = 0;
    if ((ret = mbedtls_base64_decode((unsigned char *) sig.valueHolder, MBEDTLS_MPI_MAX_SIZE, &sig.acutalSize,
                                     (unsigned char *) s.c_str(), s.size())) != 0) {
        throw std::runtime_error(string("error while decoding base64 signature " + Utils::errorDescription(ret)));
    }
    return sig;
}

