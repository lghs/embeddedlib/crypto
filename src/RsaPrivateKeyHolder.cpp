

#include <cstring>
#include "RsaPrivateKeyHolder.h"

void RsaPrivateKeyHolder::loadKey(string keyPath) {
    mbedtls_pk_init(&pk);
    int ret = 0;
    if ((ret = mbedtls_pk_parse_keyfile(&pk, keyPath.c_str(), NULL)) != 0) {
        throw std::runtime_error(string("error while loading private key " + Utils::errorDescription(ret)));
    }
    if (!mbedtls_pk_can_do(&pk, MBEDTLS_PK_RSA)) {
        throw std::runtime_error(string("not a rsa key"));
    }
}

Sha256RsaSignature RsaPrivateKeyHolder::sign(Sha256Checksum checksum) {
    Sha256RsaSignature signature;

    long ret = 0;
      if ((ret = mbedtls_pk_sign(&pk, MBEDTLS_MD_SHA256, (const unsigned char *)(checksum.value()), checksum.size(),
                                   (unsigned char *)signature.value(), &signature.acutalSize,
                                   mbedtls_ctr_drbg_random, &ctr_drbg
                                   )) != 0) {
          throw std::runtime_error((string("invalid signature ") + Utils::errorDescription(ret)).c_str());
      }
    return signature;

}

RsaPrivateKeyHolder::RsaPrivateKeyHolder() {

    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_init(&ctr_drbg);
    mbedtls_entropy_init(&entropy);

    const char* pers=MY_ENTROPY;

    mbedtls_ctr_drbg_seed(
            &ctr_drbg,
            mbedtls_entropy_func,
            &entropy,
            (const unsigned char*)pers,
            strlen(pers));
}