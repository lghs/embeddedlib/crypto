

#include <stdexcept>
#include <Utils.h>
#include "Base64.h"

string Base64::decodeString(string s) {
    size_t size = (s.size() * 3 / 4) + 4;
    char out[size];
    int ret = 0;
    size_t olen;
    if ((ret = mbedtls_base64_decode((unsigned char *) out, size, &olen,
                                     (unsigned char *) s.c_str(), s.size())) != 0) {
        throw std::runtime_error(string("error while decoding base64 signature " + Utils::errorDescription(ret)));
    }
    return string(out);
}

string Base64::encode(string s) {
    encode(s.size(), (unsigned char *) s.c_str());
}

string Base64::encode(size_t size, unsigned char *data) {
    size_t buffLength = (size * 4 / 3) + 4;
    unsigned char outputBase64[buffLength];
    size_t olen = 0;
    int ret = 0;
    if ((ret = mbedtls_base64_encode(outputBase64, buffLength, reinterpret_cast<size_t *>(&olen), data, size)) != 0) {
        throw std::runtime_error(string(Utils::errorDescription(ret)));
    }
    return string((char *) outputBase64);
}