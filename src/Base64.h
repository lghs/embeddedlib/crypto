#pragma once

#include <string>
#include <mbedtls/base64.h>

using namespace std;

class Base64 {
public:
    static string decodeString(string s);
    static string encode(string s);
    static string encode(size_t s, unsigned char* data);
};



