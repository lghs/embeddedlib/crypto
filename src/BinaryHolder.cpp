

#include <mbedtls/base64.h>
#include <stdexcept>
#include "BinaryHolder.h"
#include "Utils.h"
#include "Base64.h"

size_t BinaryHolder::size() {
    throw new std::logic_error("undefined");
}

char * BinaryHolder::value() {
    throw new std::logic_error("undefined");
}

void BinaryHolder::initBlank() {
    char *v = value();
    for(int i = 0; i< size(); i++){
        v[i] = 0;
    }
}

string BinaryHolder::base64() {
    return Base64::encode(size(), (unsigned char *)value());
}
