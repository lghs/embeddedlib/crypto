

#include <stdexcept>
#include <mbedtls/base64.h>
#include <mbedtls/sha256.h>
#include "Sha256Checksum.h"

Sha256Checksum::Sha256Checksum(char *value) {
    for (int i = 0; i < SHA256_CHECKSUM_SIZE; i++) {
        this->valueHolder[i] = value[i];
    }
};

Sha256Checksum::Sha256Checksum() {
    initBlank();
};


Sha256Checksum Sha256Checksum::of(string message) {
    Sha256Checksum checksum;
    mbedtls_sha256_ret((const unsigned char *) message.c_str(), message.length(),
                       (unsigned char *)(checksum.valueHolder), 0);
    return checksum;
}
