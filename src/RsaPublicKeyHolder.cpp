

#include "RsaPublicKeyHolder.h"


void RsaPublicKeyHolder::loadKeyFromFile(string publicKeyPath) {
    mbedtls_pk_init(&pk);
    int ret = 0;
    if ((ret = mbedtls_pk_parse_public_keyfile(&pk, publicKeyPath.c_str())) != 0) {
        throw std::runtime_error(string("error while loading public key " + Utils::errorDescription(ret)));
    }
    if (!mbedtls_pk_can_do(&pk, MBEDTLS_PK_RSA)) {
        throw std::runtime_error(string("not a rsa key"));
    }

    mbedtls_rsa_set_padding(mbedtls_pk_rsa(pk),
                            MBEDTLS_RSA_PKCS_V15,
                            MBEDTLS_MD_SHA256);
}

void RsaPublicKeyHolder::loadKey(string key) {
    mbedtls_pk_init(&pk);
    int ret = 0;
    if ((ret = mbedtls_pk_parse_public_key(&pk, (unsigned char *) key.c_str(), key.size() + 1)) != 0) {
        throw std::runtime_error(string("error while loading public key " + Utils::errorDescription(ret)));
    }
    if (!mbedtls_pk_can_do(&pk, MBEDTLS_PK_RSA)) {
        throw std::runtime_error(string("not a rsa key"));
    }

    mbedtls_rsa_set_padding(mbedtls_pk_rsa(pk),
                            MBEDTLS_RSA_PKCS_V15,
                            MBEDTLS_MD_SHA256);
}

void RsaPublicKeyHolder::verifySignature(Sha256Checksum &checksum, Sha256RsaSignature signature) {

    long ret = 0;


    if ((ret = mbedtls_pk_verify(&pk, MBEDTLS_MD_SHA256, (const unsigned char *) (checksum.value()), checksum.size(),
                                 (const unsigned char *) signature.value(), signature.size())) != 0) {
        throw std::runtime_error((string("invalid signature ") + Utils::errorDescription(ret)).c_str());
    }
}


void RsaPublicKeyHolder::verifySignature(string &m, Sha256RsaSignature signature) {
    Sha256Checksum checksum = Sha256Checksum::of(m);
    verifySignature(checksum, signature);
}
